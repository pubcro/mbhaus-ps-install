php7.3-xml - DOM, SimpleXML, WDDX, XML, and XSL module for PHP
php7.3-opcache - Zend OpCache module for PHP
php7.3-mbstring - MBSTRING module for PHP
php-yaml - YAML-1.1 parser and emitter for PHP
php7.3-intl - Internationalisation module for PHP
php7.3-mysql - MySQL module for PHP
php7.3-cli - command-line interpreter for the PHP scripting language
php7.3-pgsql - PostgreSQL module for PHP
php7.3-curl - CURL module for PHP
php7.3-sqlite3 - SQLite3 module for PHP
php7.3-bz2 - bzip2 module for PHP
php7.3-json - JSON module for PHP
php-memcache - memcache extension module for PHP
php-mongodb - MongoDB driver for PHP
php-memcached - memcached extension module for PHP, uses libmemcached
php7.3-fpm - server-side, HTML-embedded scripting language (FPM-CGI binary)
php-uploadprogress - file upload progress tracking extension for PHP
php7.3-soap - SOAP module for PHP
php-redis - PHP extension for interfacing with Redis
php-http - PECL HTTP module for PHP Extended HTTP Support
php-gmagick - Provides a wrapper to the GraphicsMagick library
php7.3-gd - GD module for PHP
php-oauth - OAuth 1.0 consumer and provider extension
php7.3-readline - readline module for PHP
php7.3-common - documentation, examples and common module for PHP
php7.3-enchant - Enchant module for PHP
php7.3-imap - IMAP module for PHP
php7.3-zip - Zip module for PHP
php7.3-dev - Files for PHP7.3 module development
php-mailparse - Email message manipulation for PHP
libapache2-mod-php7.3 - server-side, HTML-embedded scripting language (Apache 2 module)
php-geoip - GeoIP module for PHP
php7.3-phpdbg - server-side, HTML-embedded scripting language (PHPDBG binary)
php - server-side, HTML-embedded scripting language (default)
php-curl - CURL module for PHP [default]
php-recode - recode module for PHP [default]
php-tokenizer - tokenized PHP source to XML converter
php-font-lib - read, parse, export and make subsets of different fonts
php-soap - SOAP module for PHP [default]
php7.3 - server-side, HTML-embedded scripting language (metapackage)
php-gd - GD module for PHP [default]
php-intl - Internationalisation module for PHP [default]
php-fpdf - PHP class to generate PDF files
php-dev - Files for PHP module development (default)
php-cgi - server-side, HTML-embedded scripting language (CGI binary) (default)
php-getid3 - scripts to extract information from multimedia files
libapache2-mod-php - server-side, HTML-embedded scripting language (Apache 2 module) (default)
phpunit-git - Simple wrapper for Git
php-deepcopy - create deep copies (clones) of objects
php-cache-tag-interop - Framework interoperable interfaces for tags
libphp-magpierss - provides an XML-based RSS parser in PHP
php-cli - command-line interpreter for the PHP scripting language (default)
php-php-gettext - read gettext MO files directly, without requiring anything other than PHP
php-zend-code - Zend Framework - Code component
php-sql-formatter - a PHP SQL highlighting library
php-xmlrpc - XMLRPC-EPI module for PHP [default]
php-bcmath - Bcmath module for PHP [default]
php-pgsql - PostgreSQL module for PHP [default]
php-xml-svg - XML_SVG API
php-sqlite3 - SQLite3 module for PHP [default]
php-mail - Class that provides multiple interfaces for sending emails
pkg-php-tools - various packaging tools and scripts for PHP packages
php-bz2 - bzip2 module for PHP [default]
php-common - Common files for PHP packages
php-pspell - pspell module for PHP [default]
php-net-imap - Provides an implementation of the IMAP protocol
php-mysql - MySQL module for PHP [default]
php-image-text - Image_Text - Advanced text maipulations in images
php-readline - readline module for PHP [default]
php-pecl-http-dev - pecl_http module for PHP Extended HTTP Support [dummy]
php-parser - convert PHP code into abstract syntax tree
libphp-phpmailer - full featured email transfer class for PHP
php-file-iterator - FilterIterator implementation for PHP
php-mbstring - MBSTRING module for PHP [default]
php-timer - Utility class for timing
php-imap - IMAP module for PHP [default]
php-twig - Flexible, fast, and secure template engine for PHP
phpqrcode - PHP library for generating two-dimensional barcodes
php-net-url2 - Class for parsing and handling URL
php-tidy - tidy module for PHP [default]
php-fpm - server-side, HTML-embedded scripting language (FPM-CGI binary) (default)
php-json - JSON module for PHP [default]
php-auth-sasl - Abstraction of various SASL mechanism responses
php-embed - Get info from any web service or page
php7.3-xsl - XSL module for PHP (dummy)
php-xml - DOM, SimpleXML, WDDX, XML, and XSL module for PHP [default]
libphp-embed - HTML-embedded scripting language (Embedded SAPI library) (default)
php-pear - PEAR Base System
php-http-request - Provides an easy way to perform HTTP requests
php-phpdbg - server-side, HTML-embedded scripting language (PHPDBG binary) (default)
php-markdown - PHP library for rendering Markdown data
php-json-schema - implementation of JSON schema
php-net-whois - PHP PEAR module for querying whois services
php-net-idna2 - PHP Pear module for handling international domain names
php-zip - Zip module for PHP [default]
php-mail-mime - PHP PEAR module for creating MIME messages
pear-channels - PEAR channels for various projects
php-cache-lite - Fast and Safe little cache system
php-date - Generic date/time handling class for PEAR
php-pecl-http - pecl_http module for PHP Extended HTTP Support [dummy]
php-pclzip - ZIP archive manager class for PHP
php-htmlpurifier - Standards-compliant HTML filter
libphp-swiftmailer - transitional dummy package
php-net-url - Easy parsing of Urls
php-zend-stdlib - Zend Framework - Stdlib component
