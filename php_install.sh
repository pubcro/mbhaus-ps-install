#!/bin/bash
sudo apt-get update
for p in $(cat ./valid_install) ; do apt-get install -y --yes --force-yes $p ; done
a2enmod proxy_fcgi setenvif
a2enconf php7.0-fpm
for m in $(cat ./valid_modules) ; do a2enmod $m ; done
/etc/init.d/apache2 restart
apt-get autoremove
apt-get install -f
