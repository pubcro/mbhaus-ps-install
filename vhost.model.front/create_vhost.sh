for v in $(cat ./vhost) ; do
     rm -f ${v}.conf
     cat appmodel.conf | sed "s/%APP%/$v/g" > ${v}.conf
     mkdir /var/log/apache2/$v 2>/dev/null
done
exit 0
